
<?php
function endsWith($str, $sub) {
	return $sub === "" || substr ( $str, - strlen ( $sub ) ) === $sub;
}
function startsWith($str, $sub) {
	return $sub === "" || strpos ( $str, $sub ) === 0;
}
function wcurl($method, $url, $query = '', $payload = '', $request_headers = array(), &$response_headers = array(), $curl_opts = array()) {
	$ch = curl_init ( wcurl_request_uri ( $url, $query ) );
	wcurl_setopts ( $ch, $method, $payload, $request_headers, $curl_opts );
	$response = curl_exec ( $ch );
	$curl_info = curl_getinfo ( $ch );
	$errno = curl_errno ( $ch );
	$error = curl_error ( $ch );
	curl_close ( $ch );
	
	if ($errno)
		throw new Exception ( $error, $errno );
	
	$header_size = $curl_info ["header_size"];
	$msg_header = substr ( $response, 0, $header_size );
	$msg_body = substr ( $response, $header_size );
	
	$response_headers = wcurl_response_headers ( $msg_header );
	
	return $msg_body;
}
function wcurl_request_uri($url, $query) {
	if (empty ( $query ))
		return $url;
	if (is_array ( $query ))
		return "$url?" . http_build_query ( $query );
	else
		return "$url?$query";
}
function wcurl_setopts($ch, $method, $payload, $request_headers, $curl_opts) {
	$default_curl_opts = array (
			CURLOPT_HEADER => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_MAXREDIRS => 3,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_USERAGENT => 'wcurl',
			CURLOPT_CONNECTTIMEOUT => 30,
			CURLOPT_TIMEOUT => 300
	);
	
	if ('GET' == $method) {
		$default_curl_opts [CURLOPT_HTTPGET] = true;
	} else {
		$default_curl_opts [CURLOPT_CUSTOMREQUEST] = $method;
		
		// Disable cURL's default 100-continue expectation
		if ('POST' == $method)
			array_push ( $request_headers, 'Expect:' );
		
		if (! empty ( $payload )) {
			if (is_array ( $payload )) {
				$payload = http_build_query ( $payload );
				array_push ( $request_headers, 'Content-Type: application/x-www-form-urlencoded; charset=utf-8' );
			}
			
			$default_curl_opts [CURLOPT_POSTFIELDS] = $payload;
		}
	}
	
	if (! empty ( $request_headers ))
		$default_curl_opts [CURLOPT_HTTPHEADER] = $request_headers;
	
	$overriden_opts = $curl_opts + $default_curl_opts;
	foreach ( $overriden_opts as $curl_opt => $value )
		curl_setopt ( $ch, $curl_opt, $value );
}
function wcurl_response_headers($msg_header) {
	$multiple_headers = preg_split ( "/\r\n\r\n|\n\n|\r\r/", trim ( $msg_header ) );
	$last_response_header_lines = array_pop ( $multiple_headers );
	$response_headers = array ();
	
	$header_lines = preg_split ( "/\r\n|\n|\r/", $last_response_header_lines );
	list ( , $response_headers ['http_status_code'], $response_headers ['http_status_message'] ) = explode ( ' ', trim ( array_shift ( $header_lines ) ), 3 );
	foreach ( $header_lines as $header_line ) {
		list ( $name, $value ) = explode ( ':', $header_line, 2 );
		$response_headers [strtolower ( $name )] = trim ( $value );
	}
	
	return $response_headers;
}
function install_url($shop, $api_key) {
	return "http://$shop/admin/api/auth?api_key=$api_key";
}
function is_valid_request($query_params, $shared_secret) {
	$seconds_in_a_day = 24 * 60 * 60;
	$older_than_a_day = $query_params ['timestamp'] < (time () - $seconds_in_a_day);
	if ($older_than_a_day)
		return false;
	
	$signature = $query_params ['signature'];
	unset ( $query_params ['signature'] );
	
	foreach ( $query_params as $key => $val )
		$params [] = "$key=$val";
	sort ( $params );
	
	return (md5 ( $shared_secret . implode ( '', $params ) ) === $signature);
}
function permission_url($shop, $api_key, $scope = array(), $redirect_uri = '') {
	$scope = empty ( $scope ) ? '' : '&scope=' . implode ( ',', $scope );
	$redirect_uri = empty ( $redirect_uri ) ? '' : '&redirect_uri=' . urlencode ( $redirect_uri );
	return "https://$shop/admin/oauth/authorize?client_id=$api_key$scope$redirect_uri";
}
function oauth_access_token($shop, $api_key, $shared_secret, $code) {
	return _api ( 'POST', "https://$shop/admin/oauth/access_token", NULL, array (
			'client_id' => $api_key,
			'client_secret' => $shared_secret,
			'code' => $code 
	) );
}
function client($shop, $shops_token, $api_key, $shared_secret, $private_app = false) {
	$password = $shops_token;
	$baseurl = "https://$shop/";
	
	return function ($method, $path, $params = array(), &$response_headers = array()) use($baseurl, $shops_token) {
		$url = $baseurl . ltrim ( $path, '/' );
		$query = in_array ( $method, array (
				'GET',
				'DELETE' 
		) ) ? $params : array ();
		$payload = in_array ( $method, array (
				'POST',
				'PUT' 
		) ) ? stripslashes ( json_encode ( $params ) ) : array ();
		// print_r($payload);
		$request_headers = array ();
		array_push ( $request_headers, "X-Shopify-Access-Token: $shops_token" );
		if (in_array ( $method, array (
				'POST',
				'PUT' 
		) ))
			array_push ( $request_headers, "Content-Type: application/json; charset=utf-8" );
		
		return _api ( $method, $url, $query, $payload, $request_headers, $response_headers );
	};
}
function _api($method, $url, $query = '', $payload = '', $request_headers = array(), &$response_headers = array(), $loop = 0) {
	try {
		$response = wcurl ( $method, $url, $query, $payload, $request_headers, $response_headers );
	} catch ( Exception $e ) {
		throw new CurlException ( $e->getMessage (), $e->getCode () );
	}
	
	$response = json_decode ( $response, true );
	if (isset ( $response ['errors'] ) or ($response_headers ['http_status_code'] >= 400)) {
		echo 'response_headers_status:' . json_encode ( $response_headers ) . PHP_EOL;
		if ($loop < 3 && ($response_headers ['http_status_code'] == 429 || calls_left ( $response_headers ) == 0)) {
			usleep ( 500000 ); // sleep 0.5 second and try again (max 3 times)
			$loop ++;
			return _api ( $method, $url, $query, $payload, $request_headers, $response_headers, $loop );
		}
		throw new ApiException ( compact ( 'method', 'path', 'params', 'response_headers', 'response', 'shops_myshopify_domain', 'shops_token' ) );
	}
	
	if (calls_left ( $response_headers ) > 0 && calls_left ( $response_headers ) <= 3) {
		usleep ( 100000 );
	}
	return (is_array ( $response ) and ! empty ( $response )) ? array_shift ( $response ) : $response;
}
function calls_made($response_headers) {
	return _shop_api_call_limit_param ( 0, $response_headers );
}
function call_limit($response_headers) {
	return _shop_api_call_limit_param ( 1, $response_headers );
}
function calls_left($response_headers) {
	if (calls_made ( $response_headers ) >= 0 && call_limit ( $response_headers ) > 0) {
		return call_limit ( $response_headers ) - calls_made ( $response_headers );
	} else {
		return - 1;
	}
}
function _shop_api_call_limit_param($index, $response_headers) {
	try {
		if (isset ( $response_headers ['http_x_shopify_shop_api_call_limit'] )) {
			$params = explode ( '/', $response_headers ['http_x_shopify_shop_api_call_limit'] );
			return ( int ) $params [$index];
		}
	} catch ( Exception $e ) {
		echo json_encode ( $e ) . PHP_EOL;
	}
	return - 1;
}
class CurlException extends \Exception {
}
class ApiException extends \Exception {
	protected $info;
	function __construct($info) {
		$this->info = $info;
		parent::__construct ( $info ['response_headers'] ['http_status_message'], $info ['response_headers'] ['http_status_code'] );
	}
	function getInfo() {
		return $this->info;
	}
}
function legacy_token_to_oauth_token($shops_token, $shared_secret, $private_app = false) {
	return $private_app ? $secret : md5 ( $shared_secret . $shops_token );
}
function legacy_baseurl($shop, $api_key, $password) {
	return "https://$api_key:$password@$shop/";
}
function getCurrentThemeID($shopify, $store_name) {
	$result = array ();
	$themes = $shopify ( 'GET', '/admin/themes.json', array (
			'fields' => "id,name,processing,role" 
	) );
	foreach ( $themes as $theme ) {
		if ($theme ['role'] == 'main' || $theme ['role'] == 'mobile') {
			$result [] = $theme ['id'];
			echo json_encode ( $theme ) . PHP_EOL;
			// break;
		}
	}
	return $result;
}


$flag = true;
$for_counter = 0;
$cart_quantity_edited = false;
$cart_url_edited = false;
$cart_title_edited = false;
$variant_edited = false;
$option_edited = false;
$current_file = "abc";
$isbuyx = false;
$is_set_option =  false;
$title_edited = false;
$form_edited = false;
$is_option_break_line = false;
$current_product = "product";
$json_product_partern = "##@##";
$is_form_page = false;
$form_edited = false;
$json_product = "{{ product | json }}";
$is_product_page = false;
$current_variant = "variant";
function variant_filter($contents) {
	
	$contents = explode("\n", $contents);
	global $current_product,$json_product_partern,$json_product,$current_variant;
	$current_variant = "variant";
	$contents = array_map ( function ($content) {
		global $current_file,$flag, $for_counter, $finename, $cart_quantity_edited, $cart_url_edited, $cart_title_edited,$variant_edited,$option_edited,$isbuyx,
		$is_option_break_line,$is_set_option,$json_product,$json_product_partern,$is_product_page,$is_form_page,$current_product,$title_edited,$form_edited,$current_variant;
		if ($flag) {
			$regex = "/({%.*?for (.*?) in (.*?).variants.*%})/";
		} else {
			$regex = "/({% endfor %})/";
		}
		
		if (preg_match ( $regex, $content ,$matches)) {
			if ($regex == "/({%.*?for (.*?) in (.*?).variants.*%})/"){
				$current_product = $matches[3];
				$current_variant = $matches[2];
			}
			$variant_edited = true;
			
			if (preg_match ( '/(^.*?{%.*?for (.*?) in (.*?).variants.*?%})(.*?)({%.*?endfor.*)/', $content ,$matches_n)) {
				$content = $matches_n[1].PHP_EOL.'{% unless '.$matches_n[2].'.metafields.secomapp.freegifts %}' . PHP_EOL . "{% unless ".$matches_n[2].".title contains '(Freegifts)' %}" . PHP_EOL.$matches_n[4];
				$content = $content.PHP_EOL . '{%endunless%}' . PHP_EOL . '{%endunless%}' . PHP_EOL . $matches_n[5];
			} else {
				if ($flag) {
					$content = $content . PHP_EOL . '{% unless '.$current_variant.'.metafields.secomapp.freegifts %}' . PHP_EOL . "{% unless ".$current_variant.".title contains '(Freegifts)' %}" . PHP_EOL;
				} else if ($for_counter == 0) {
					$content = PHP_EOL . '{%endunless%}' . PHP_EOL . '{%endunless%}' . PHP_EOL . $content;
				} else {
					-- $for_counter;
					return $content;
				}
				
				$flag = ! $flag;
			}
			
			
		} else if (strpos ( $content, "{% for" ) && ! $flag) {
			++ $for_counter;
		}
		
		if (preg_match ( '/(cart)|(theme)|(header)/', $current_file )) {
			
			if (preg_match ( "/(.*?link_to.*?).*?(:*.?item.url.*?)(}}.*)/", $content,$match_url )) {
				$content = preg_replace ( "/(:*.?item.url.*?)}/", ":sca_url }", $content );
				$content  = '{%assign sca_url = '.str_replace(":","",$match_url[2]).' %} {%if item.variant.metafields.secomapp.freegifts%} {%assign sca_url = item.product.url  %}{%endif%}'.PHP_EOL.$content;
				
				$cart_url_edited = true;
			} else	if (preg_match ( "/({{.*?item.url.*?}})/", $content )) {
				$content = preg_replace ( "/({{.*?item.url.*?}})/", "  {%if item.variant.metafields.secomapp.freegifts%}{{ item.product.url }}{%else%}$1{%endif%}", $content );
				$cart_url_edited = true;
			}
			if (preg_match ( '/({{.?item((\.t)|(\.v)).*?itle)(.*?}})/', $content )) {
				$content = preg_replace ( '/({{.?item((\.t)|(\.v)).*?itle)(.*?}})/', "$1 | remove: '/ Default Title'| remove: 'Default Title' | remove: '/ Default' | remove: 'Default' | replace: '(Freegifts)', '' $5", $content );
				$cart_title_edited = true;
			}
			
			if (preg_match ( '/(^.*?<input)( .*?name.*?updates.*)/', $content )) {
				$content = preg_replace ( '/(^.*?<input)( .*?name.*?updates.*)/', "$1 {%if item.variant.metafields.secomapp.freegifts%}readonly{%endif%}  $2", $content );
				$cart_quantity_edited = true;
			}
			
		}
		
		if (preg_match ( '/(function buyx_product_json.*{\z)/', $content )) {
			$content = $content.PHP_EOL.'var freegifts_product_json=function(a){if(a){String.prototype.endsWith||Object.defineProperty(String.prototype,"endsWith",{value:function(a,b){var c=this.toString();(void 0===b||b>c.length)&&(b=c.length),b-=a.length;var d=c.indexOf(a,b);return-1!==d&&d===b}});for(var b=function(a){for(var b in a)if(a.hasOwnProperty(b))return!1;return!0},c=a.price,d=a.price_max,e=a.price_min,f=a.compare_at_price,g=a.compare_at_price_max,h=a.compare_at_price_min,i=0;i<a.variants.length;i++){var j=a.variants[i],k=null!==j.option3?j.option3:null!==j.option2?j.option2:j.option1;"undefined"!=typeof SECOMAPP&&"undefined"!=typeof SECOMAPP.gifts_list_avai&&!b(SECOMAPP.gifts_list_avai)&&"undefined"!=typeof SECOMAPP.gifts_list_avai[j.id]||k.endsWith("(Freegifts)")||k.endsWith("% off)")?(a.variants.splice(i,1),i-=1):(d>=j.price&&(d=j.price,c=j.price),e<=j.price&&(e=j.price),j.compare_at_price&&(g>=j.compare_at_price&&(g=j.compare_at_price,f=j.compare_at_price),h<=j.compare_at_price&&(h=j.compare_at_price)),1==j.available&&(a.available=!0))}a.price=c,a.price_max=e,a.price_min=d,a.compare_at_price=f,a.compare_at_price_max=h,a.compare_at_price_min=g,a.price_varies=e>d?!0:!1,a.compare_at_price_varies=h>g?!0:!1}return a};';
			$content = $content.PHP_EOL.'product = freegifts_product_json ( product );'.PHP_EOL;
			$isbuyx = true;
		}
		
		if ( preg_match ( '/(^.*?Shopify\.OptionSelectors.*?product:.*?)({{.*?}})/', $content ,$matches)) {
			$option_edited = true;
			$content = preg_replace ( '/(^.*?Shopify\.OptionSelectors.*?product:.*?)({{.*?}})/', "$1sca_product_info", $content );
			$content = 'var sca_product_info = freegifts_product_json ( '.$matches[2].' );'.PHP_EOL.$content;
			$content = 'var freegifts_product_json=function(a){if(a){String.prototype.endsWith||Object.defineProperty(String.prototype,"endsWith",{value:function(a,b){var c=this.toString();(void 0===b||b>c.length)&&(b=c.length),b-=a.length;var d=c.indexOf(a,b);return-1!==d&&d===b}});for(var b=function(a){for(var b in a)if(a.hasOwnProperty(b))return!1;return!0},c=a.price,d=a.price_max,e=a.price_min,f=a.compare_at_price,g=a.compare_at_price_max,h=a.compare_at_price_min,i=0;i<a.variants.length;i++){var j=a.variants[i],k=null!==j.option3?j.option3:null!==j.option2?j.option2:j.option1;"undefined"!=typeof SECOMAPP&&"undefined"!=typeof SECOMAPP.gifts_list_avai&&!b(SECOMAPP.gifts_list_avai)&&"undefined"!=typeof SECOMAPP.gifts_list_avai[j.id]||k.endsWith("(Freegifts)")||k.endsWith("% off)")?(a.variants.splice(i,1),i-=1):(d>=j.price&&(d=j.price,c=j.price),e<=j.price&&(e=j.price),j.compare_at_price&&(g>=j.compare_at_price&&(g=j.compare_at_price,f=j.compare_at_price),h<=j.compare_at_price&&(h=j.compare_at_price)),1==j.available&&(a.available=!0))}a.price=c,a.price_max=e,a.price_min=d,a.compare_at_price=f,a.compare_at_price_max=h,a.compare_at_price_min=g,a.price_varies=e>d?!0:!1,a.compare_at_price_varies=h>g?!0:!1}return a};'.PHP_EOL.$content;
			
		} else if ($is_option_break_line && preg_match ( '/(Shopify.OptionSelectors.*?{\z)/', $content ,$matches)) {
			$content = 'var sca_product_info = freegifts_product_json ('.$json_product_partern.');'.PHP_EOL.$content;
			$content = 'var freegifts_product_json=function(a){if(a){String.prototype.endsWith||Object.defineProperty(String.prototype,"endsWith",{value:function(a,b){var c=this.toString();(void 0===b||b>c.length)&&(b=c.length),b-=a.length;var d=c.indexOf(a,b);return-1!==d&&d===b}});for(var b=function(a){for(var b in a)if(a.hasOwnProperty(b))return!1;return!0},c=a.price,d=a.price_max,e=a.price_min,f=a.compare_at_price,g=a.compare_at_price_max,h=a.compare_at_price_min,i=0;i<a.variants.length;i++){var j=a.variants[i],k=null!==j.option3?j.option3:null!==j.option2?j.option2:j.option1;"undefined"!=typeof SECOMAPP&&"undefined"!=typeof SECOMAPP.gifts_list_avai&&!b(SECOMAPP.gifts_list_avai)&&"undefined"!=typeof SECOMAPP.gifts_list_avai[j.id]||k.endsWith("(Freegifts)")||k.endsWith("% off)")?(a.variants.splice(i,1),i-=1):(d>=j.price&&(d=j.price,c=j.price),e<=j.price&&(e=j.price),j.compare_at_price&&(g>=j.compare_at_price&&(g=j.compare_at_price,f=j.compare_at_price),h<=j.compare_at_price&&(h=j.compare_at_price)),1==j.available&&(a.available=!0))}a.price=c,a.price_max=e,a.price_min=d,a.compare_at_price=f,a.compare_at_price_max=h,a.compare_at_price_min=g,a.price_varies=e>d?!0:!1,a.compare_at_price_varies=h>g?!0:!1}return a};'.PHP_EOL.$content;
			$is_set_option =  true;;
		} else if ($is_option_break_line && $is_set_option && preg_match ( '/(.*?product:.*?)(({{.*?}}))/', $content ,$matches)) {
			$option_edited = true;
			$is_set_option = false;
			$content = preg_replace ( '/(.*?product:).*?(({{.*?}}))/', "$1sca_product_info", $content );
			$json_product = $matches[2];
		} 
		
		if (preg_match('/(Shopify.linkOptionSelectors.*?\().*?({{.*?}})/',$content)) {
			$content = preg_replace ( '/(Shopify.linkOptionSelectors.*?\(.*?)({{.*?}})/', "$1sca_product_info", $content );
		}
		
		if ($is_product_page && preg_match ( '/((<h.*?>)(.*?{{.*?(.*?).title.*?}}.*?)(<.*?>))/', $content ,$matches)) {
			$content = preg_replace ( '/((<h.*?>)(.*?{{.*?(.*?).title.*?}}.*?)(<.*?>))/', '$2<div style="position:relative;"> $3 <div name="secomapp-fg-image-{{ $4.id }}" style="display: none;"> {{ "icon-freegift.png" | asset_url | img_tag: "Free Gift", "sca-fg-img-label" }} </div> </div>$5', $content );
			$title_edited = true;
		}
		
		if (preg_match('/(<form.*?cart\/add)/',$content)) {
			$is_form_page = true;
		}
		
		if ($is_form_page && $variant_edited && preg_match('/(<.*?form>)/',$content)) {
			$content = $content.PHP_EOL.'<div class="sca-fg-cat-list" style="display: none;" name="secomapp-fg-data-{{ '.$current_product.'.id }}"> </div>'.PHP_EOL;
			$is_form_page = false;
			$form_edited = true;
		}
		return $content;
	}, $contents );
	
	$contents = implode ( "\n", $contents );
	return str_replace( $json_product_partern,$json_product,$contents);
}
function filter_content_liquid_new($file, $content, $is_item, $is_variants, $price_words) {
	$file = '@' . $file;
	
	if (substr ( $file, - 7 ) == '.liquid') {
		
		if ($is_variants && preg_match ( '/({%.*?for (.*?) in (.*?)\.variants.*%})/', $content ) || preg_match ( '/(cart)|(theme)|(header)/', $file )) {
				global $current_file,$flag, $for_counter, $finename, $cart_quantity_edited, $cart_url_edited, $cart_title_edited,$variant_edited,$option_edited,$isbuyx,
		$is_option_break_line,$is_set_option,$json_product,$json_product_partern,$is_product_page,$is_form_page,$current_product,$title_edited,$form_edited;
			$flag = true;
			$for_counter = 0;
			$cart_quantity_edited = false;
			$cart_url_edited = false;
			$cart_title_edited = false;
			$variant_edited = false;
			$option_edited = false;
			$isbuyx = false;
			$is_set_option =  false;
			$title_edited = false;
			$current_product = "product";
			$json_product_partern = "##@##";
			$is_form_page = false;
			$form_edited = false;
			$json_product = "{{ product | json }}";
			$current_file = $file;
			$is_product_page = strpos($content,"schema.org/Product");
			$is_option_break_line = strpos($content,"new Shopify.OptionSelectors");
			
			$content = variant_filter ( $content );
			if ($variant_edited) {
				$file = "variants_" . $file;
			} 
			
			if ($cart_quantity_edited) {
				$file = "quantity_" . $file;
			}
			if ($cart_url_edited) {
				$file = "url_" . $file;
			}
			if ($cart_title_edited) {
				$file = "title_" . $file;
			}
			
			if ($option_edited) {
				$file = "option_" . $file;
			}
			
			if ($isbuyx) {
				$file = "buyx_".$file;				
			}
			
			if ($title_edited) {
				$file = "Ptitle_".$file;
			}
			
			if ($form_edited) {
				$file = "form_".$file;
			}
		}
		
		
		$price_regex = "/";
		$temp_parterm = "(@.avai)|(@.price)|(@.compare)|(@.variants.size)|";
		foreach ( $price_words as $word ) {
			$price_regex .= str_replace ( "@", $word, $temp_parterm );
		}
		
		$price_regex .= '/';
		$price_regex = str_replace ( '|/', '/', $price_regex );
		// $price_regex = '/(product-block.avai)|(product-block.price)|(product-block.compare)|(product-block.variants.size)|(product.avai)|(product.price)|(product.compare)|(product.variants.size)|(prod.avai)|(prod.price)|(prod.compare)|(prod.variants.size)|(item.avai)|(item.product.avai)|(item.product.price)|(item.price)|(item.product.compare)|(item.compare)|(item.product.variants.size)|(item.variants.size)/';
		$price_item_regex = '/(item\.avai)|(item\.product\.avai)|(item\.product\.price)|(item\.price)|(item\.product\.compare)|(item\.compare)|(item\.product\.variants\.size)|(item\.variants\.size)/';
		preg_match ( $price_item_regex, $content, $matches_item );
		$is_price_item = false;
		if (sizeof ( $matches_item ) > 0) {
			$is_price_item = true;
		}
		
		preg_match ( $price_regex, $content, $matches );
		$is_edit = false;
		if (sizeof ( $matches ) > 0) {
			$is_edit = true;
			$file = 'price_' . $file;
		}
		
		if ($is_edit) {
			
			$edited = false;
			foreach ( $price_words as $word ) {
				if (preg_match ( '/(' . $word . '\.avai)|(' . $word . '\.price)|(' . $word . '\.compare)|(' . $word . '\.variants\.size)/', $content ) && ! $is_price_item) {
					$content = str_replace ( $word . ".avai", "sca_product_avai", $content );
					$content = str_replace ( $word . ".price", "sca_price", $content );
					$content = str_replace ( $word . ".compare", "sca_compare", $content );
					$content = str_replace ( $word . ".variants.size", "sca_product_variantCount", $content );
					$content = str_replace ( $word . ".variants | size", "sca_product_variantCount", $content );
					$file = $word . '_' . $file;
					$edited = true;
					break;
				}
			}
			
			if (! $edited && $is_item) {
				$content = str_replace ( "item.avai", "sca_product_avai", $content );
				if (strpos ( $content, 'item.product.price' )) {
					$content = str_replace ( "item.product.price", "sca_price", $content );
				} else {
					$content = str_replace ( "item.price", "sca_price", $content );
				}
				
				if (strpos ( $content, 'item.product.compare' )) {
					$content = str_replace ( "item.product.compare", "sca_compare", $content );
				} else {
					$content = str_replace ( "item.compare", "sca_compare", $content );
				}
				
				if (strpos ( $content, 'item.product.variants.size' )) {
					$content = str_replace ( "item.product.variants.size", "sca_product_variantCount", $content );
				} else {
					$content = str_replace ( "item.variants.size", "sca_product_variantCount", $content );
				}
				
				if (strpos ( $content, 'item.variants | size' )) {
					$content = str_replace ( "item.variants | size", "sca_product_variantCount", $content );
				} else {
					$content = str_replace ( "item.product.variants | size", "sca_product_variantCount", $content );
				}
				
				$file = 'item' . '_' . $file;
			}
		}
	}
	
	$result = array (
			$file,
			$content 
	);
	return $result;
}
function filter_content_liquid($file, $content, $is_item, $is_product, $is_prod, $match_price_condition) {
	$file = '@' . $file;
	$is_product_block = strpos ( $content, 'product-block.' );
	if (substr ( $file, - 7 ) == '.liquid') {
		$price_regex = '/(product-block.avai)|(product-block.price)|(product-block.compare)|(product-block.variants.size)|(product.avai)|(product.price)|(product.compare)|(product.variants.size)|(prod.avai)|(prod.price)|(prod.compare)|(prod.variants.size)|(item.avai)|(item.product.avai)|(item.product.price)|(item.price)|(item.product.compare)|(item.compare)|(item.product.variants.size)|(item.variants.size)/';
		$price_item_regex = '/(item.avai)|(item.product.avai)|(item.product.price)|(item.price)|(item.product.compare)|(item.compare)|(item.product.variants.size)|(item.variants.size)/';
		preg_match ( $price_item_regex, $content, $matches_item );
		$is_price_item = false;
		if (sizeof ( $matches_item ) > 0) {
			$is_price_item = true;
		}
		
		preg_match ( $price_regex, $content, $matches );
		$is_edit = false;
		if (sizeof ( $matches ) > 0) {
			$is_edit = true;
			$file = 'price_' . $file;
		}
		
		if ($is_edit) {
			if ($is_product_block && ! $is_price_item) {
				$content = str_replace ( "product-block.avai", "sca_product_avai", $content );
				$content = str_replace ( "product-block.price", "sca_price", $content );
				$content = str_replace ( "product-block.compare", "sca_compare", $content );
				$content = str_replace ( "product-block.variants.size", "sca_product_variantCount", $content );
			} else if ($is_product && ! $is_price_item) {
				$content = str_replace ( "product.avai", "sca_product_avai", $content );
				$content = str_replace ( "product.price", "sca_price", $content );
				$content = str_replace ( "product.compare", "sca_compare", $content );
				$content = str_replace ( "product.variants.size", "sca_product_variantCount", $content );
			} else if ($is_prod && ! $is_price_item) {
				$content = str_replace ( "prod.avai", "sca_product_avai", $content );
				$content = str_replace ( "prod.price", "sca_price", $content );
				$content = str_replace ( "prod.compare", "sca_compare", $content );
				$content = str_replace ( "prod.variants.size", "sca_product_variantCount", $content );
			} else if ($is_item) {
				$content = str_replace ( "item.avai", "sca_product_avai", $content );
				if (strpos ( $content, 'item.product.price' )) {
					$content = str_replace ( "item.product.price", "sca_price", $content );
				} else {
					$content = str_replace ( "item.price", "sca_price", $content );
				}
				
				if (strpos ( $content, 'item.product.compare' )) {
					$content = str_replace ( "item.product.compare", "sca_compare", $content );
				} else {
					$content = str_replace ( "item.compare", "sca_compare", $content );
				}
				
				if (strpos ( $content, 'item.product.variants.size' )) {
					$content = str_replace ( "item.product.variants.size", "sca_product_variantCount", $content );
				} else {
					$content = str_replace ( "item.variants.size", "sca_product_variantCount", $content );
				}
			}
		}
	}
	
	$result = array (
			$file,
			$content 
	);
	return $result;
}
function file_filter($dir, $themeidFolder, $file, $contents) {
    if (isIgnoreFile($file)) return;
	$price_words = array (
			"selectedProduct",
			"itemType",
			"product-label",
			"product-detail",
			"product-block",
	        'wh_calculate_discount_no_js',
	        'wh_calculate_discount',
			"fprod",
			"nprod",
			'prod',
			"product",
			'item.product',
			'item'
	        
	);
	$is_variants = strpos ( $contents, '.variants %}' );
	$is_item = (strpos ( $contents, 'item.' ));
	$is_secomapp_file = preg_match ( '/(\.sca\.)|(\.css)/', $file );
	$match_normal_condition = preg_match ( '/(\.vari)|(\.avai)|(\.price)|(\.compare)|(\.variants\.size)|(\.url)|(\| size)/', $contents );
	$json_add = preg_match ( '/(\| json)|(add\.js)/', $contents ) ;
	$all_products = preg_match ( '/({{.*?all_products.*?[.*?].*?}})/', $contents ) ;
	$product_regex = '/';
	
	foreach ( $price_words as $word ) {
		if (strpos ( $word, '.' )) {
			$word = str_replace ( '.', '\.', $word );
		}
		$product_regex .= '(' . $word . '\.)|';
	}
	$product_regex .= '/';
	$product_regex = str_replace ( '|/', '/', $product_regex );
	$match_product_condition = preg_match ( $product_regex, $contents );
	
	if (($json_add || $is_variants || ($match_normal_condition && $match_product_condition)) && ! $is_secomapp_file) {
		if ($all_products) {
			$file = 'allpr@'.$file;
		}
		if ($json_add) {
			file_put_contents ( $dir . DIRECTORY_SEPARATOR . 'SCAEDIT_#@' . $file, $contents );
		} else  {
			file_put_contents ( $dir . DIRECTORY_SEPARATOR . 'SCAEDIT_@' . $file, $contents );
		}
		
		
		$result = filter_content_liquid_new ( $file, $contents, $is_item, $is_variants, $price_words );
		if (! file_exists ( $themeidFolder . DIRECTORY_SEPARATOR . 'SCAEDIT_' . $result [0] )) {
			if ($json_add) {
				file_put_contents ( $themeidFolder . DIRECTORY_SEPARATOR . 'SCAEDIT_#' . $result [0], $result [1] );
			} else {
				file_put_contents ( $themeidFolder . DIRECTORY_SEPARATOR . 'SCAEDIT_' . $result [0], $result [1] );
			}
			
		}
	}
}
function file_force_contents($dir, $contents) {
	$parts = explode ( DIRECTORY_SEPARATOR, $dir );
	$file = array_pop ( $parts );
	print $dir;
	$shopifyDir = $parts [(sizeof ( $parts ) - 1)];
	$dir = '';
	$themeidFolder = '';
	foreach ( $parts as $part ) {
		if (strpos ( $part, 'myshopify.com' )) {
			$parts2 = explode ( '_', $part );
			
			$themeidFolder = $dir . DIRECTORY_SEPARATOR . $parts2 [0];
			if (! is_dir ( $themeidFolder ))
				mkdir ( $themeidFolder );
			
			$themeidFolder = $themeidFolder . DIRECTORY_SEPARATOR . $parts2 [1];
			if (! is_dir ( $themeidFolder ))
				mkdir ( $themeidFolder );
			$themeidFolder = $themeidFolder . DIRECTORY_SEPARATOR . $shopifyDir;
			if (! is_dir ( $themeidFolder ))
				mkdir ( $themeidFolder );
		}
		
		if (! is_dir ( $dir .= ($part . DIRECTORY_SEPARATOR) )) {
			echo 'make dir:' . $dir . PHP_EOL;
			mkdir ( $dir );
		}
	}
	file_put_contents ( $dir . DIRECTORY_SEPARATOR . $file, $contents );
	file_filter ( $dir, $themeidFolder, $file, $contents );
}

function isIgnoreFile ($file_name) {
    $filter = array(
        'collection.sca.productid.js.liquid',
        'index.sca.collection.js.liquid',
        'index.sca.product_type.js.liquid',
        'index.sca.vendor.js.liquid',
        'search.sca.freegifts.json.liquid',
        'search.sca.product.js.liquid',
        'search.sca.productid.js.liquid',
        'search.sca.variant.js.liquid',
        'sca.freegift.css',
        'icon-freegift.png',
        'sca.freegifts.liquid',
        'sca.freegifts.api.js',
        'sca.freegifts.status.liquid',
        'sca_freegift_price.liquid',
    );
    
    return in_array($file_name, $filter);
}

function cloneTheme($shopify, $store_name, $themeid) {
	$currentDir = getcwd ();
	$date = new DateTime ( 'UTC' );
	$revision = $date->format ( 'YmdHis' );
	$theme = $shopify ( 'GET', '/admin/themes/' . $themeid . '/assets.json', array () );
	foreach ( $theme as $asset ) {
		echo json_encode ( $asset ) . PHP_EOL;
		if (startsWith ( $asset ['key'], 'templates' ) || endsWith ( $asset ['key'], '.css' ) || endsWith ( $asset ['key'], '.js' ) || endsWith ( $asset ['key'], '.json' ) || endsWith ( $asset ['key'], '.liquid' ) || endsWith ( $asset ['key'], '.html' )) {
			$contents = $shopify ( 'GET', '/admin/themes/' . $themeid . '/assets.json', array (
					'asset[key]' => $asset ['key'],
					'theme_id' => $themeid 
			) );
			$asset ['key'] = str_replace ( "/", DIRECTORY_SEPARATOR, $asset ['key'] );
			echo $asset ['key'] . PHP_EOL; // .$contents['value'].PHP_EOL;
			file_force_contents ( $currentDir . DIRECTORY_SEPARATOR . $store_name . '_' . $themeid . '_' . $revision . DIRECTORY_SEPARATOR . $asset ['key'], $contents ['value'] );
		}
	}
}
function upload($themeid, $shopify, $store_name) {
	$currentDir = getcwd () . DIRECTORY_SEPARATOR . $store_name . DIRECTORY_SEPARATOR . $themeid;
	$listDir = scandir ( $currentDir );
	foreach ( $listDir as $dir ) {
		if ($dir == '.' || $dir == '..')
			continue;
		$dirpath = $currentDir . DIRECTORY_SEPARATOR . $dir;
		foreach ( scandir ( $dirpath ) as $file ) {
			if ($file == '.' || $file == '..')
				continue;
			$filename = $dir . '/' . preg_replace ( '/(SCAEDIT_.*@)/', '', $file );
			
			$filepath = $dirpath . DIRECTORY_SEPARATOR . $file;
			$myfile = fopen ( $filepath, "r" ) or die ( "Unable to open file!" );
			$content = fread ( $myfile, filesize ( $filepath ) );
			$updateTheme = array (
					"asset" => array (
							"key" => $filename,
							"attachment" => base64_encode ( $content ) 
					) 
			);
			
			$asset = $shopify ( 'PUT', '/admin/themes/' . $themeid . '/assets.json', $updateTheme, $response_headers );
			print_r ( 'asset->key:' . $asset ['key'] . PHP_EOL );
			print_r ( 'asset->updated_at:' . $asset ['updated_at'] . PHP_EOL );
		}
	}
}

// starting here
$store_name = "dung1.myshopify.com";
$token_key = "7192de00fbd0b16692114ecdb1b62fcd";
$shopify = client ( $store_name, $token_key, "181c217c862c543710a39e03910a2c86", "3da7d719192213653380e7cbca1fa183" );

$store_path = getcwd () . DIRECTORY_SEPARATOR . $store_name . DIRECTORY_SEPARATOR;
function download($shopify, $store_name) {
	$themes = getCurrentThemeID ( $shopify, $store_name );
	echo json_encode ( $themes ) . PHP_EOL;
	foreach ( $themes as $themeid ) {
		cloneTheme ( $shopify, $store_name, $themeid );
		
		$finename = "/Users/dungdv/Zend/workspaces/DefaultWorkspace11/fgsupport/sca_freegift_price.liquid";
		$myfile = fopen ( $finename, "r" ) or die ( "Unable to open file!" );
		$content = fread ( $myfile, filesize ( $finename ) );
		$updateTheme = array (
				"asset" => array (
						"key" => "snippets/sca_freegift_price.liquid",
						"attachment" => base64_encode ( $content ) 
				) 
		);
		$asset = $shopify ( 'PUT', '/admin/themes/' . $themeid . '/assets.json', $updateTheme, $response_headers );
		print_r ( 'asset->key:' . $asset ['key'] . PHP_EOL );
		print_r ( 'asset->updated_at:' . $asset ['updated_at'] . PHP_EOL );
	}
}

download ( $shopify, $store_name );
//upload to theme  
//upload(4576116,$shopify,$store_name);

